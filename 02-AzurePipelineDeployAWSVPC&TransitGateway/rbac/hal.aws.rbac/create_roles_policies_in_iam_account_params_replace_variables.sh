tenantshortname=$1
awstenantaccount=$2
sed -i "s/\$(VAR_TENANT_SHORT_NAME)/$tenantshortname/" create_roles_policies_in_iam_account_params.json
sed -i "s/\$(VAR_TENANT_ACCOUNT)/$awstenantaccount/" create_roles_policies_in_iam_account_params.json