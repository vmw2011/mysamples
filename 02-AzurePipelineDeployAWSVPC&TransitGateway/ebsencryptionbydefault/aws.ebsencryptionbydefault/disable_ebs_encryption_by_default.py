# This AWS Lambda function automatically disables EBS_Encryption_By_Default 
# settings in all regions within an given AWS account.
# Environment Requirement:
# - Python 2.7
# - Boto3 version 1.9.165 or later (https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
# - Use a virtual environment to install dependencies for your function
# - Refer to https://docs.aws.amazon.com/lambda/latest/dg/lambda-python-how-to-create-deployment-package.html#python-package-venv
# - Define Lambda function name as disable_ebs_encryption_by_default
# - Set timeout to 30+ seconds
from datetime import datetime

import json
import boto3
import cfnresponse

def lambda_handler(event, context):
    stack = event['StackId']

    # initiate client for EC2
    client = boto3.client('ec2')

    # retrieve live regions
    regions = client.describe_regions()

    # retrieve account number
    sclient = boto3.client('sts')
    awsacct = sclient.get_caller_identity()['Account']

    responses = []
    # loop in regions
    for region in regions['Regions']: 
        rclient = boto3.client('ec2', region['RegionName'])
        response = rclient.disable_ebs_encryption_by_default()
        print('[%s]: EBS_Encryption_By_Default is %s in %s' %(awsacct, response['EbsEncryptionByDefault'], region['RegionName']))
        responses.append(response)
    
    try:
        cfnresponse.send(event, context, cfnresponse.SUCCESS, {}, "{} metrics".format(stack))
    except Exception as e:
        cfnresponse.send(event, context, cfnresponse.FAILED, {
            "Data": str(e),
        }, "{} metrics".format(stack))