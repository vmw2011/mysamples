newtenantname=${1}
awstgwid=${2}
prodvpccidr=${3}
prodtgwattachment=${4}
devvpccidr=${5}
devtgwattachment=${6}
testvpccidr=${7}
testtgwattachment=${8}
ssvpccidr=${9}
sstgwattachment=${10}

echo $ProdVPCTGWAttachment
echo $DevVPCTGWAttachment
echo $TestVPCTGWAttachment

sed -i "s^\$(New_Tenant_Name)^$newtenantname^" create_rules_in_tgwrt_params.json
sed -i "s^\$(AWS_TGW_ID)^$awstgwid^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Product_VPC_CIDR)^$prodvpccidr^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Product_TGW_Attachment)^$prodtgwattachment^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Dev_VPC_CIDR)^$devvpccidr^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Dev_TGW_Attachment)^$devtgwattachment^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Test_VPC_CIDR)^$testvpccidr^" create_rules_in_tgwrt_params.json
sed -i "s^\$(Test_TGW_Attachment)^$testtgwattachment^" create_rules_in_tgwrt_params.json
sed -i "s^\$(SS_VPC_CIDR)^$ssvpccidr^" create_rules_in_tgwrt_params.json
sed -i "s^\$(SS_TGW_Attachment)^$sstgwattachment^" create_rules_in_tgwrt_params.json