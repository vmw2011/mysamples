cfnstackid=${1}
awstgwid=${2}
prodvpccidr=${3}
devvpccidr=${4}
testvpccidr=${5}
appstreamvpccidr=${6}

sed -i "s^\$(CFN_Stack_ID)^$cfnstackid^" create_route_in_tenant_vpcs_params.json
sed -i "s^\$(AWS_TGW_ID)^$awstgwid^" create_route_in_tenant_vpcs_params.json
sed -i "s^\$(Product_VPC_CIDR)^$prodvpccidr^" create_route_in_tenant_vpcs_params.json
sed -i "s^\$(Dev_VPC_CIDR)^$devvpccidr^" create_route_in_tenant_vpcs_params.json
sed -i "s^\$(Test_VPC_CIDR)^$testvpccidr^" create_route_in_tenant_vpcs_params.json
sed -i "s^\$(AppStream_VPC_CIDR)^$appstreamvpccidr^" create_route_in_tenant_vpcs_params.json