newtenantname=${1}
prodvpccidr=${2}
prodSubnet1CIDR=${3}
prodSubnet2CIDR=${4}
devvpccidr=${5}
devSubnet1CIDR=${6}
devSubnet2CIDR=${7}
testvpccidr=${8}
testSubnet1CIDR=${9}
testSubnet2CIDR=${10}
networkAWSAccount=${11}
awstgwid=${12}
awss3endpoint=${13}
apvpccidr=${14}

sed -i "s^\$(New_Tenant_Name)^$newtenantname^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Product_VPC_CIDR)^$prodvpccidr^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Product_Subnet1_CIDR)^$prodSubnet1CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Product_Subnet2_CIDR)^$prodSubnet2CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Dev_VPC_CIDR)^$devvpccidr^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Dev_Subnet1_CIDR)^$devSubnet1CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Dev_Subnet2_CIDR)^$devSubnet2CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Test_VPC_CIDR)^$testvpccidr^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Test_Subnet1_CIDR)^$testSubnet1CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Test_Subnet2_CIDR)^$testSubnet2CIDR^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(Network_AWS_Account)^$networkAWSAccount^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(AWS_TGW_ID)^$awstgwid^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(AWS_S3_Endpoint)^$awss3endpoint^" provision_all_new_tenant_vpcs_params.json
sed -i "s^\$(AppStream_VPC_CIDR)^$apvpccidr^" provision_all_new_tenant_vpcs_params.json