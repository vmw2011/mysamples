# This template creates VPC routes that are from subnet CIDR to TGW
# CloudFormation is not supported TGW as target when creating
# new route as the time this Lambda function is created.
#
# Sample Input:
#
# {
#    "RouteParameters": [
#        {
#            "DestinationCidrBlock":"10.12.4.0/24",
#            "TransitGatewayId": "tgw-04xxxxxe8fa68a7dc",
#            "RouteTableId": "rtb-06b8exxxxx68c3a7b"
#        }
#    ]
# }
#
# Note:
# An Amazon S3 bucket in the same AWS Region as your function. The bucket can be in a different AWS account.
# https://docs.amazonaws.cn/en_us/AWSCloudFormation/latest/UserGuide/aws-properties-lambda-function-code.html

import json
import boto3
import cfnresponse

def lambda_handler(event, context):
    # TODO implement
    stack = event['StackId']
    try:
        
        client = boto3.client('ec2')
        
        response = client.create_route(
                DestinationCidrBlock=event['ResourceProperties']['DestinationCidrBlock'],
                TransitGatewayId=event['ResourceProperties']['TransitGatewayId'],
                RouteTableId=event['ResourceProperties']['RouteTableId'],
        )
        
        print(response)
        
        cfnresponse.send(event, context, cfnresponse.SUCCESS, {}, "{} metrics".format(stack))    
    except Exception as e:
        cfnresponse.send(event, context, cfnresponse.FAILED, {
            "Data": str(e),
        }, "{} metrics".format(stack))