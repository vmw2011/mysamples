$functionCode = ""
$environmentCode = ""
$productLineCode = ""
$customerCode = ""
$serviceCode = ""

# Input 1: Provide the tenant VPCs CIDR
$prodVPCIp = ""
$devVPCIp = ""
$testVPCIp = ""

# Input 2: Set tenant VPCs' subnets CIDR range
$oct1,$oct2,$oct3,$oct4 = $prodVPCIp.Split('.')
$oct4,$net = $oct4.Split('/')
$productsubnet1cidr = "$oct1.$oct2.$oct3.0/25"
$productsubnet2cidr = "$oct1.$oct2.$oct3.128/25"
$oct1,$oct2,$oct3,$oct4 = $devVPCIp.Split('.')
$oct4,$net = $oct4.Split('/')
$devsubnet1cidr = "$oct1.$oct2.$oct3.0/25"
$devsubnet2cidr = "$oct1.$oct2.$oct3.128/25"
$oct1,$oct2,$oct3,$oct4 = $testVPCIp.Split('.')
$oct4,$net = $oct4.Split('/')
$testsubnet1cidr = "$oct1.$oct2.$oct3.0/25"
$testsubnet2cidr = "$oct1.$oct2.$oct3.128/25"

# Input 3: set AWS varialbes
$awsRegion = ""
$awsS3Endpoint = ""
$tenantshortname = ""
$tenantAWSAccount = ""
$networkAWSAccount = ""
$sharedServicesAWSAccount = ""
$iamAWSAccount = ""

# Input 4: Set AppStream transit gateway attachment Id
$appstreamVPCIp = ""
$appstreamtgwattachmentid = "d"

# Input 5: Provide Transit Gateway ID in us-east-1 region
$awsTGWId = ""

# Set configuration file name
$configfilename = "$functionCode-$customerCode-$productLineCode-$environmentCode-$serviceCode-config.json"

$vars = [ordered]@{}

$vars.Add("Product_VPC_CIDR", $prodVPCIp)
$vars.Add("Product_Subnet1_CIDR", $productsubnet1cidr)
$vars.Add("Product_Subnet2_CIDR", $productsubnet2cidr)
$vars.Add("Dev_VPC_CIDR", $devVPCIp)
$vars.Add("Dev_Subnet1_CIDR", $devsubnet1cidr)
$vars.Add("Dev_Subnet2_CIDR", $devsubnet2cidr)
$vars.Add("Test_VPC_CIDR", $testVPCIp)
$vars.Add("Test_Subnet1_CIDR", $testsubnet1cidr)
$vars.Add("Test_Subnet2_CIDR", $testsubnet2cidr)
$vars.Add("AppStream_VPC_CIDR", $appstreamVPCIp)
$vars.Add("AppStream_TGW_Attachment", $appstreamtgwattachmentid)
$vars.Add("AWS_Region", $awsRegion)
$vars.Add("AWS_TGW_ID", $awsTGWId)
$vars.Add("Tenant_Short_Name", $tenantshortname)
$vars.Add("Tenant_AWS_Account", $tenantAWSAccount)
$vars.Add("Network_AWS_Account", $networkAWSAccount)
$vars.Add("SharedServices_AWS_account", $sharedServicesAWSAccount)
$vars.Add("AWS_IAM_Account", $iamAWSAccount)
$vars.Add("AWS_S3_Endpoint", $awsS3Endpoint)

$configfilename = $configfilename.ToLower()

$vars | ConvertTo-Json -depth 10
$vars | ConvertTo-Json -depth 10 | out-file $configfilename -Encoding OEM
write-host "Config file created: $configfilename"