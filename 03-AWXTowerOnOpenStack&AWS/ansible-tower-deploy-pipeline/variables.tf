variable "provider" {}
variable "region" {}
variable "org_id" {}
variable "env" {}
variable "access_key" {}
variable "secret_access_key" {}
variable "private_subnet_id" {}
variable "public_subnet_id" {}
variable "vpc_id" {}
variable "alb_id" {}
variable "hosted_zone_id" {}
variable "hosted_zone_domain" {}
variable "datestamp" {}
variable "infra_tier_sg_id" {}
variable "data_tier_sg_id" {}
variable "ds_apps_tier_sg_id" {}
variable "horizon_view_tier_sg_id" {}
variable "trusted_user_ca_keys" {}

# OpenStack required parameters
variable "op_username" {
  default = ""
}

variable "op_password" {
  default = "*UNSET*"
}

variable "os_auth_url" {
  default = "https://sample.com:5000/v3"
}

variable "image_name" {
  default = ""
}

variable "image_id"{
  default = ""
}

variable "jenkins_server_cider" {
  default = ""
}

variable "op_network_id" {
    default = ""
}

variable "op_tenant_name" {
    default = ""
}

variable "flavor_name" {
  default = "3d.xlarge"
  description = "OpenStack Instance type"
}
variable "key_name" {
  default = ""
}