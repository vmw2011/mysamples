output "ansible_tower_public_ip" {
  value = "${element(concat(aws_instance.ansible.*.public_ip, list("")), 0)}"
}
