data "aws_ami" "awx_centos7_base" {
  count       = "${var.enable == "true" ? 1 : 0}"
  most_recent = true

  filter {
    name   = "name"
    values = ["awx-base-centos-7-3-v*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["704737384310"]
}

data "aws_vpc" "current_vpc" {
  count = "${var.enable == "true" ? 1 : 0}"
  id = "${var.vpc_id}"
}

resource "aws_instance" "ansible" {
  count                       = "${var.enable == "true" ? 1 : 0}"
  ami                         = "${data.aws_ami.awx_centos7_base.id}"
  key_name                    = "${var.key_name}"
  vpc_security_group_ids      = ["${aws_security_group.ansible.id}", "${var.infra_tier_sg_id}", "${var.data_tier_sg_id}", "${var.ds_apps_tier_sg_id}", "${var.horizon_view_tier_sg_id}"]
  subnet_id                   = "${var.public_subnet_id}"
  instance_type               = "${var.aws_instance_type}"
  associate_public_ip_address = "true"
  user_data                   = "${data.template_file.userdata.rendered}"

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.vm_ansible_disk_size}"
    delete_on_termination = true
  }

  tags {
    Name         = "${lower(var.org_id)}-${var.env}-${var.application}-${var.datestamp}"
    Env          = "${var.env}"
    Application  = "${var.application}"
    Component    = "${var.application}"
    CreationDate = "${var.datestamp}"
    OS           = "${var.vm_os}"
    SSHusername  = "centos"
  }

  lifecycle {
    ignore_changes = ["description", "Name", "name", "CreationDate"]
  }
}

# For AWSPEC tests.  Will loop for each of 3 servers and then will validate the last one.
resource "local_file" "awspec-tests-variables" {
  count = "${var.enable == "true" ? 1 : 0}"

  content = <<EOS
  export AWS_SECURITY_GROUP=${aws_security_group.ansible.id}
  export AWS_SUBNET_ID=${var.public_subnet_id}
  export AWS_AMI_ID=ami-c2586dbd
  export AWS_APPLICATION=${var.application}
  export AWS_COMPONENT=${var.application}
  export AWS_OS=${var.vm_os}
  export AWS_ENV=${var.env}
  EOS

  filename = "${path.module}/tests/tf.vars"
}

resource "aws_security_group" "ansible" {
  count       = "${var.enable == "true" ? 1 : 0}"
  name        = "${var.org_id}-${var.env}-ansible"
  description = "Security group for Ansible Tower instance"
  vpc_id      = "${var.vpc_id}"

  // Temporary
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["34.235.122.34/32"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["18.209.105.104/32"]
  }

  // This is for outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// This is temporary code to allow ansible access to local gitlab
resource "aws_security_group_rule" "local_gitlab_allow_ansible" {
  count       = "${var.enable == "true" ? 1 : 0}"
  type        = "ingress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["${aws_instance.ansible.0.public_ip}/32"]

  security_group_id = "sg-ed6269a4"
}

resource "aws_security_group_rule" "local_vault_allow_ansible" {
  count       = "${var.enable == "true" ? 1 : 0}"
  type        = "ingress"
  from_port   = 8200
  to_port     = 8200
  protocol    = "tcp"
  cidr_blocks = ["${aws_instance.ansible.0.public_ip}/32"]

  security_group_id = "sg-377eba7c"
}

data "template_file" "userdata" {
  count    = "${var.enable == "true" ? 1 : 0}"
  template = "${file("${path.module}/scripts/user_data.sh")}"

  vars {
    trusted_user_ca_keys = "${var.trusted_user_ca_keys}"
  }
}
