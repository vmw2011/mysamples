variable enable {}

variable datestamp {
  default = "*UNSET*"
}

# Common variables used in multiple pipeline

variable org_id {
  description = "org_id ID"
}

variable "region" {}

variable env {
  description = "Deployment environment"
}

variable "access_key" {}
variable "secret_access_key" {}
variable "private_subnet_id" {}
variable "public_subnet_id" {}
variable "vpc_id" {}
variable "infra_tier_sg_id" {}
variable "data_tier_sg_id" {}
variable "ds_apps_tier_sg_id" {}
variable "horizon_view_tier_sg_id" {}
variable "hosted_zone_id" {}
variable "hosted_zone_domain" {}

variable key_name {
  default = ""

  description = "SSH key name in your AWS account for AWS instances."
}

variable aws_instance_type {
  default = "t2.large"

  description = "AWS Instance type, if you change, make sure it is compatible with AMI, not all AMIs allow all instance types "
}

variable application {
  default = "ansible"

  description = "Application"
}

variable vm_os {
  default = "Centos"

  description = "VM O/S"
}

variable "jenkins_server_ip" {
  default = ""
}

variable "vm_ansible_disk_size" {
  default = 200
}

variable "alb_id" {}

variable "trusted_user_ca_keys" {}
