#!/usr/bin/env bash
sudo yum remove -y git
cd /tmp
sudo wget https://centos7.iuscommunity.org/ius-release.rpm
sudo rpm -Uvh ius-release*rpm
sudo yum install -y git2u
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce
sudo systemctl start docker
sudo pip install docker-py
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
sudo yum -y install nodejs
mkdir /home/centos/Landmark
mkdir /home/centos/awx_web
mkdir /home/centos/awx_task
mkdir /home/centos/identities
mkdir /pgdocker
sudo mkdir -p /var/lib/awx/projects
sudo chmod -R 777 /var/lib/awx/projects

cd /tmp
git clone http://18.212.174.230/khoatdd/awx.git
cd /tmp/awx/installer
sudo ansible-playbook -i inventory build.yml