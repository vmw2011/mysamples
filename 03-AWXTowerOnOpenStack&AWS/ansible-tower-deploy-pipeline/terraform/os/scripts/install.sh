#!/usr/bin/env bash

cd /tmp	
sudo yum install -y ansible

sudo wget https://centos7.iuscommunity.org/ius-release.rpm  
sudo rpm -Uvh ius-release*rpm
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce
sudo systemctl start docker
sudo pip install --upgrade pip
sudo pip install docker-py

sudo yum remove -y git
sudo yum install -y curl-devel expat-devel gettext-devel openssl-devel zlib-devel
sudo wget https://github.com/git/git/archive/v2.20.0.tar.gz
sudo tar -zxf v2.20.0.tar.gz
cd git-2.20.0
sudo make configure
sudo ./configure --prefix=/usr
sudo make all
sudo make install

cd /tmp
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
sudo yum -y install nodejs

sudo mkdir /home/centos/Landmark
sudo mkdir /home/centos/awx_web
sudo mkdir /home/centos/awx_task
sudo mkdir /home/centos/identities
sudo mkdir /pgdocker
sudo mkdir -p /var/lib/awx/projects
sudo chmod -R 777 /var/lib/awx/projects