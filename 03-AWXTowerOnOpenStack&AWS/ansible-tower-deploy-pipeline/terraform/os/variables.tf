variable enable {
  default = "true"
}

variable datestamp {
  default = "*UNSET*"
}

# Common variables used in multiple pipeline
variable org_id {
  description = "org_id ID"
}

variable "region" {}

variable env {
  description = "Deployment environment"
}

variable "op_network_id" {} // Used as OpenStack network ID, e.g. 63ac9050-a090-4444-a81f-30e3755c8658
variable "op_tenant_name" {
  default = "Onica"
}
variable "infra_tier_sg_id" {}
variable "data_tier_sg_id" {}
variable "ds_apps_tier_sg_id" {}
variable "horizon_view_tier_sg_id" {}

variable key_name {
  default = "DF04_Automation"

  description = "SSH key name in your OpenStack account for OpenStack instances."
}

variable "flavor_name" {
  default = "2d.large"
  description = "OpenStack Instance type"
}

variable application {
  default = "ansible"

  description = "Application"
}

variable vm_os {
  default = "Centos"

  description = "VM O/S"
}

variable "jenkins_server_ip" {
  default = ""
}

variable "vm_ansible_disk_size" {
  default = 20
}


variable "trusted_user_ca_keys" {}

# OpenStack required parameters
variable "op_username" {
}

variable "op_password" {
  
}

variable "os_auth_url" {
  default = "https://sample.com:5000/v3"
}

variable "image_name" {
  default = ""
}

variable "image_id"{
  default = ""
}

variable "jenkins_server_cider" {
  default = ""
}