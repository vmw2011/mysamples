provider "openstack" {
  user_name   = "${var.op_username}"
  tenant_name = "${var.op_tenant_name}"
  password    = "${var.op_password}"
  auth_url    = "${var.os_auth_url}"
  region      = "${var.enable == "true" ? var.region : "nova"}"
}