data "openstack_images_image_v2" "ansible-instance-image" {
    count                       = "${var.enable == "true" ? 1 : 0}" 

    name                        = "${var.image_name}"
    most_recent                 = true
}

resource "openstack_compute_instance_v2" "ansible_instance" {
    depends_on                  = ["openstack_networking_secgroup_rule_v2.ansible_instance_sg_rule-jenkins_ip_all" ]

    count                       = "${var.enable == "true" ? 1 : 0}"  
    
    name                        = "ansible_instance"
    flavor_name                 = "${var.flavor_name}"
    key_pair                    = "${var.key_name}"
    security_groups             = ["${openstack_networking_secgroup_v2.ansible_instance_sg.name}", "${var.infra_tier_sg_id}", "${var.data_tier_sg_id}", "${var.ds_apps_tier_sg_id}", "${var.horizon_view_tier_sg_id}"] 
    image_id                    = "${data.openstack_images_image_v2.ansible-instance-image.id}"
    network {
        uuid                    = "${var.op_network_id}"
    }

    block_device {
        source_type           = "image"
        boot_index            = 0
        destination_type      = "local"
        delete_on_termination = true
        uuid                  = "${data.openstack_images_image_v2.ansible-instance-image.id}"
    }


    metadata {
        Name         = "${lower(var.org_id)}-${var.env}-${var.application}-${var.datestamp}"
        Env          = "${var.env}"
        Application  = "${var.application}"
        Component    = "${var.application}"
        CreationDate = "${var.datestamp}"
        OS           = "${var.vm_os}"
        SSHusername  = "centos"
    }
}

resource "openstack_networking_secgroup_v2" "ansible_instance_sg" {
    count                       = "${var.enable == "true" ? 1 : 0}"  

    name                        = "${var.org_id}-${var.env}-ansible_instance_sg"
    description                 = "Security group for Ansible Tower instance"
}

resource "openstack_networking_secgroup_rule_v2" "ansible_instance_sg_rule-jenkins_ip_all" {
    count                       = "${var.enable == "true" ? 1 : 0}"  

    depends_on                  = ["openstack_networking_secgroup_v2.ansible_instance_sg"]
    direction                   = "ingress"
    ethertype                   = "IPv4"
    protocol                    = "tcp"
    port_range_min              = 1
    port_range_max              = 65535
    remote_ip_prefix            = "${var.jenkins_server_cider}"
    security_group_id           = "${openstack_networking_secgroup_v2.ansible_instance_sg.id}"
}
