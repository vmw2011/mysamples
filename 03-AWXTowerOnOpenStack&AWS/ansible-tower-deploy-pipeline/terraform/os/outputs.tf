output "ansible_tower_public_ip" {
  value = "${element(concat(openstack_compute_instance_v2.ansible_instance.*.network.0.fixed_ip_v4, list("")), 0)}"
}
