terraform {
  required_version = ">= 0.11.3"

  backend "s3" {
    bucket = "riggit-ienergy-terraform-state"
    key    = "file_path"
    region = "us-east-1"
  }
}
