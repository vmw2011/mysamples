# Environment Variable : TF_VAR_op_username
variable "op_username" {

}

variable "op_kube_tenant_name" {
  default = ""
}

# Environment Variable : TF_VAR_op_password
variable "op_password" {
  
}

variable "op_kube_auth_url" {
  default = "https://proxy.sample.com:5000/v3"
}

variable "host_flavor_name" {
  default = "2d.xsmall"
}

variable "node_flavor_name" {
  default = "2d.medium"
}

variable "image_name" {
  default = ""
}

variable "image_id"{
  default = ""
}

variable "region_name" {
  default = "nova"
}

variable "authorized_keys_file_name" {
  default = "op-kube-kp"
}

variable "op_kube_sg_name" {
  default = "op-kube-sg"
}

variable "pool" {
  default = "private"
}

variable "op_kube_exist_network_name" {
  default = "tenant-net-landmark-rd"
}

variable "op_kube_exist_network_id" {
  default = ""
}

variable "op_kube_host_network_port_name" {
  default = "op-kube-host-network-port"
}

variable "op_kube_configuration_shell" {
  default = "op-kube-configuration.sh"
}

variable "op_kube_host_name" {
  default = "op-kube-host"
}

variable "op_kube_master_name" {
  default = "op-kube-master"
}

variable "op_kube_node_name" {
  default = "op-kube-node"
}

variable "op_kube_master_number" {
  default = 3
}

variable "op_kube_node_number" {
  default = 2
}

variable "op_kube_default_security_group" {
  default = ""
}

variable "op_kube_required_terraform_version" {
  default = "0.11.10"
}

variable "op_kube_backend_s3_bucket_name" {
  default = ""
}

variable "op_kube_backend_s3_access_credentail" {
  default = "kubespray_openstack"
}

variable "op_kube_backend_tfstate_name" {
  default = "op-kube-terraform.tfstate"
}