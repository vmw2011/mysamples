resource "openstack_networking_secgroup_v2" "op-kube-sg" {
  name        = "${var.op_kube_sg_name}"
  description = "Security group for the Kubespray instances"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3389
  port_range_max    = 3389
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3389
  port_range_max    = 3389
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3389
  port_range_max    = 3389
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5896
  port_range_max    = 5896
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5896
  port_range_max    = 5896
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5896
  port_range_max    = 5896
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}

resource "openstack_networking_secgroup_rule_v2" "op-kube-sg-xxx" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = ""
  security_group_id = "${openstack_networking_secgroup_v2.op-kube-sg.id}"
}