#!/bin/bash

echo '[Task 1] proxy = http://proxy.sample.com:80 >> /etc/yum.conf'
sudo bash -c "echo 'proxy = http://proxy.sample.com:80' >> /etc/yum.conf"
sudo bash -c "echo 'http_caching=none' >> /etc/yum.conf"

echo '[Task 2] http_proxy=http://proxy.sample.com:80 >> /etc/environment'
sudo bash -c "echo 'http_proxy=http://proxy.sample.com:80' >> /etc/environment"

echo '[Task 3] https_proxy=http://proxy.sample.com:80 >> /etc/environment'
sudo bash -c "echo 'https_proxy=http://proxy.sample.com:80' >> /etc/environment"

sudo sed -i 's/#no_proxy=127.0.0.1,localhost,169.254.169.254/' /etc/environment

echo '[Task 4] git clone git@xxxxxxxx/openstack-kubespray.git'
chmod 700 ~/.ssh && \
touch ~/.ssh/config && \
chmod 600 ~/.ssh/config && \
touch ~/.ssh/known_hosts && \
chmod 644 ~/.ssh/known_hosts && \
echo '# Configure GitLab ssh' > ~/.ssh/config && \
echo 'Host git.openearth.community' >> ~/.ssh/config && \
echo 'RSAAuthentication yes' >> ~/.ssh/config && \
echo 'IdentityFile ~/.ssh/op-kube-kp.pem' >> ~/.ssh/config && \
mv ~/op-kube-kp.pem ~/.ssh/op-kube-kp.pem && \
chmod 400 ~/.ssh/op-kube-kp.pem && \
ssh-keyscan -H git.openearth.community > ~/.ssh/known_hosts && \
cat ~/.ssh/config && \
cat ~/.ssh/known_hosts && \
cd ~ && \
git clone git@git.openearth.community:OnicaOpenStack/openstack-kubespray.git

# Yum install ius-release.rpm, python36u (with python36u-libs), python36u-devel, python36u-pip
echo '[Task 5] Install python and pip 3.6' 
cd /tmp && \
sudo wget https://centos7.iuscommunity.org/ius-release.rpm && \
sudo yum localinstall -y ius-release.rpm >> /dev/null 2>&1 && \
sudo yum install -y python36u && \
sudo yum install -y python36u-devel && \
sudo yum install -y python36u-pip && \
cd ~

echo '[Task 6] Install Python 3.6'
sudo yum install -y gcc
sudo yum install -y openssl-devel
sudo yum install -y bzip2-1.0.6
cd /usr/src
sudo wget https://www.python.org/ftp/python/3.6.6/Python-3.6.6.tgz
sudo tar -xzvf Python-3.6.6.tgz
cd Python-3.6.6
sudo ./configure --enable-optimizations
sudo make altinstall
sudo rm -f /usr/src/Python-3.6.6.tgz

echo '[Task 7] Install Kubespray requirements'
cd ~/openstack-kubespray/
sudo pip3.6 install -r requirements.txt


