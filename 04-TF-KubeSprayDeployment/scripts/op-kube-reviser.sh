
#!/bin/bash

rm -f ~/.ssh/config && \
rm -f ~/.ssh/known_hosts && \
rm -f ~/.ssh/op-kube-kp.pem && \
rm -rf ~/openstack-kubespray

sudo yum remove -y ius-release && \
sudo yum remove -y python36u-pip && \
sudo yum remove -y python36u-devel && \
sudo yum remove -y python36u

sudo rm -rf /usr/src/Python-3.6.6
sudo yum remove -y bzip2-1.0.6
sudo yum remove -y openssl-devel
sudo yum remove -y gcc


# Lastly remove the proxy

sudo sed -i '/http_proxy=http:\/\/proxy.sample.com:80/d' /etc/environment
sudo sed -i '/https_proxy=http:\/\/proxy.sample.com:80/d' /etc/environment
sudo sed -i '/proxy = http:\/\/proxy.sample.com:80/d' /etc/yum.conf
