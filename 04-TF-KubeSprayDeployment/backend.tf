
provider "aws" {
    region = "${var.region_name}"
}

terraform {
  required_version = "0.11.10"

  backend "s3" {
    bucket = ""
    key = "op-kube-terraform.tfstate"   
    region = ""
    profile = "kubespray_openstack"
  }
}