
# Display Result
resource "null_resource" "op-kube-master-show-result" {
  depends_on = ["null_resource.op-kube-master-create-sa" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1

  provisioner "remote-exec" {
    inline = [
      "echo ''",
      "echo '****************************************************************'",
      "echo '*    Terraform provisioned KubeSpray cluster                   *'",
      "echo '****************************************************************'",
      "echo ''",
      "echo 'kubectl cluster-info'",
      "kubectl cluster-info",
      "echo ''",
      "echo 'kubectl get pods --all-namespaces'",
      "kubectl get pods --all-namespaces",
      "echo ''",
      "echo 'kubectl get nodes'",
      "kubectl get nodes",
      "echo ''",
      "echo 'kubectl get serviceAccounts'",
      "kubectl get serviceAccounts"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-master.0.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}