
# Configure KubeSpray hosts.ini
resource "null_resource" "op-kube-host-ini-all-host-ip" {

  depends_on = [ "openstack_compute_instance_v2.op-kube-host", "openstack_compute_instance_v2.op-kube-master", "openstack_compute_instance_v2.op-kube-nodes", "null_resource.op-kube-host-configuration" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "echo '[all]' > /tmp/hosts.ini",
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-all-master-ip" {

  depends_on = [ "null_resource.op-kube-host-ini-all-host-ip" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_master_number}"
  
  provisioner "remote-exec" {
    inline = [
      "echo 'master${count.index+1} ansible_host=${element(openstack_compute_instance_v2.op-kube-master.*.access_ip_v4, count.index)} ip=${element(openstack_compute_instance_v2.op-kube-master.*.access_ip_v4, count.index)}' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-all-nodes-ip" {

  depends_on = [ "null_resource.op-kube-host-ini-all-master-ip" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_node_number}"
  
  provisioner "remote-exec" {
    inline = [
      "echo 'node${count.index+1} ansible_host=${element(openstack_compute_instance_v2.op-kube-nodes.*.access_ip_v4, count.index)} ip=${element(openstack_compute_instance_v2.op-kube-nodes.*.access_ip_v4, count.index)}' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-kube-master-header" {

  depends_on = [ "null_resource.op-kube-host-ini-all-nodes-ip" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "echo '' >> /tmp/hosts.ini",
      "echo '[kube-master]' >> /tmp/hosts.ini",
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-kube-masters" {

  depends_on = [ "null_resource.op-kube-host-ini-kube-master-header" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_master_number}"
  
  provisioner "remote-exec" {
    inline = [
      "echo 'master${count.index+1}' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-etcd-header" {

  depends_on = [ "null_resource.op-kube-host-ini-kube-masters" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "echo '' >> /tmp/hosts.ini",
      "echo '[etcd]' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-etcds" {

  depends_on = [ "null_resource.op-kube-host-ini-etcd-header" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_master_number}"
  
  provisioner "remote-exec" {
    inline = [
      "echo 'master${count.index+1}' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-kube-node-header" {

  depends_on = [ "null_resource.op-kube-host-ini-etcds" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "echo '' >> /tmp/hosts.ini",
      "echo '[kube-node]' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-kube-nodes" {

  depends_on = [ "null_resource.op-kube-host-ini-kube-node-header" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_node_number}"
  
  provisioner "remote-exec" {
    inline = [
      "echo 'node${count.index+1}' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-k8s-cluster-calico-rr" {

  depends_on = [ "null_resource.op-kube-host-ini-kube-nodes" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "echo '' >> /tmp/hosts.ini",
      "echo '[k8s-cluster:children]' >> /tmp/hosts.ini",
      "echo 'kube-master' >> /tmp/hosts.ini",
      "echo 'kube-node' >> /tmp/hosts.ini",
      "echo '' >> /tmp/hosts.ini",
      "echo '[calico-rr]' >> /tmp/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "null_resource" "op-kube-host-ini-cp" {

  depends_on = [ "null_resource.op-kube-host-ini-k8s-cluster-calico-rr" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1
  
  provisioner "remote-exec" {
    inline = [
      "cp /tmp/hosts.ini ~/openstack-kubespray/inventory/mycluster/hosts.ini",
      "cat ~/openstack-kubespray/inventory/mycluster/hosts.ini"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}