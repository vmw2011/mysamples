data "openstack_networking_network_v2" "op-kube-exist-network" {
  name              = "${var.op_kube_exist_network_name}"
  network_id        = "${var.op_kube_exist_network_id}"
  region            = "${var.region_name}"
}
