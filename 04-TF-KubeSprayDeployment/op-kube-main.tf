# Configure the OpenStack Provider
provider "openstack" {
  user_name   = "${var.op_username}"
  tenant_name = "${var.op_kube_tenant_name}"
  password    = "${var.op_password}"
  auth_url    = "${var.op_kube_auth_url}"
  region      = "${var.region_name}"
}

resource "openstack_compute_instance_v2" "op-kube-host" {

  depends_on = [ "openstack_networking_secgroup_v2.op-kube-sg", "data.openstack_networking_network_v2.op-kube-exist-network" ]
  
  count = 1

  name            = "${var.op_kube_host_name}"
  image_name      = "${var.image_name}"
  image_id        = "${var.image_id}"
  flavor_name     = "${var.host_flavor_name}"
  key_pair        = "${var.authorized_keys_file_name}"
  security_groups = ["${openstack_networking_secgroup_v2.op-kube-sg.name}"]  
  
  network {
    uuid = "${data.openstack_networking_network_v2.op-kube-exist-network.id}"
  }

  power_state = "active"
}

resource "openstack_compute_instance_v2" "op-kube-master" {

  depends_on = [ "openstack_compute_instance_v2.op-kube-host" ]
  
  count = "${var.op_kube_master_number}"

  name            = "${format("%s-%d", var.op_kube_master_name, count.index+1)}" # count.index starts from zero, and master is the first node
  image_name      = "${var.image_name}"
  image_id        = "${var.image_id}"
  flavor_name     = "${var.node_flavor_name}"
  key_pair        = "${var.authorized_keys_file_name}"
  security_groups = ["${openstack_networking_secgroup_v2.op-kube-sg.name}"]  

  network {
    uuid = "${data.openstack_networking_network_v2.op-kube-exist-network.id}"
  }

  power_state = "active"
}

# Update masters /etc/environment, /etc/yum.conf
resource "null_resource" "op-kube-master-update-configuration" {

  depends_on = [ "openstack_compute_instance_v2.op-kube-master" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = "${var.op_kube_master_number}"

  provisioner "remote-exec" {
    inline = [
      "sudo sed -i 's/#no_proxy=127.0.0.1,localhost,169.254.169.254/no_proxy=127.0.0.1,localhost,169.254.169.254/' /etc/environment",
      "sudo bash -c \"echo 'http_caching=none' >> /etc/yum.conf\""
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${element(openstack_compute_instance_v2.op-kube-master.*.network.0.fixed_ip_v4, count.index)}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

resource "openstack_compute_instance_v2" "op-kube-nodes" {

  depends_on = [ "openstack_compute_instance_v2.op-kube-master" ]

  count = "${var.op_kube_node_number}"

  name            = "${format("%s-%d", var.op_kube_node_name, count.index+1)}" # count.index starts from zero, and master is the first node
  image_name      = "${var.image_name}"
  image_id        = "${var.image_id}"
  flavor_name     = "${var.node_flavor_name}"
  key_pair        = "${var.authorized_keys_file_name}"
  security_groups = ["${openstack_networking_secgroup_v2.op-kube-sg.name}"]  

  network {
    uuid = "${data.openstack_networking_network_v2.op-kube-exist-network.id}"
  }

  power_state = "active"
}

resource "null_resource" "op-kube-host-files-upload" {

  depends_on = [ "openstack_compute_instance_v2.op-kube-host" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1

  provisioner "file" {
    source      = "scripts/op-kube-configuration.sh"
    destination = "/tmp/op-kube-configuration.sh"

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }

  provisioner "file" {
    source      = "keys/${var.authorized_keys_file_name}.pem"
    destination = "/home/centos/${"${var.authorized_keys_file_name}.pem"}"

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }

  provisioner "file" {
    source      = "scripts/Onica-openrc.sh"
    destination = "/tmp/Onica-openrc.sh"

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

# Configure KubeSpray Host
resource "null_resource" "op-kube-host-configuration" {
  depends_on = [ "openstack_compute_instance_v2.op-kube-host", "null_resource.op-kube-master-update-configuration", "openstack_compute_instance_v2.op-kube-nodes", "null_resource.op-kube-host-files-upload" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1

  provisioner "remote-exec" {
    inline = [
      "dos2unix /tmp/op-kube-configuration.sh",
      "chmod +x /tmp/op-kube-configuration.sh",
      "/tmp/op-kube-configuration.sh",
      "dos2unix /tmp/Onica-openrc.sh",
      "sed -i 's/read -sr OS_PASSWORD_INPUT/OS_PASSWORD_INPUT=${var.op_password}/' /tmp/Onica-openrc.sh",
      "cat /tmp/Onica-openrc.sh"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}


# Run ansible-playbook
resource "null_resource" "op-kube-host-ansible-playbook" {
  depends_on = ["null_resource.op-kube-host-configuration", "null_resource.op-kube-host-ini-cp" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1

  provisioner "remote-exec" {
    inline = [
      "cd ~/openstack-kubespray/",
      "source /tmp/Onica-openrc.sh",
      "ansible-playbook -i inventory/mycluster/hosts.ini -e @inventory/mycluster/custom_vars.yml --become --become-user=root -u centos --private-key ~/.ssh/op-kube-kp.pem cluster.yml 2>&1 | tee create_cluster.log"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-host.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}

# Create kubernets service account
resource "null_resource" "op-kube-master-create-sa" {
  depends_on = ["null_resource.op-kube-host-ansible-playbook" ]

  # Changes to host of Kubespray requires re-provivsioning
  triggers = {
    cluster_instance_ids = "${openstack_compute_instance_v2.op-kube-host.id}"
  }

  count = 1

  provisioner "remote-exec" {
    inline = [
      "cd ~",
      "kubectl create serviceaccount cluster-admin-dashboard-sa",
      "kubectl create clusterrolebinding cluster-admin-dashboard-sa --clusterrole=cluster-admin --serviceaccount=default:cluster-admin-dashboard-sa"
    ]

    connection {
      private_key = "${file("keys/${var.authorized_keys_file_name}.pem")}"
      host        = "${openstack_compute_instance_v2.op-kube-master.0.network.0.fixed_ip_v4}"
      type        = "ssh"
      user        = "centos"
    }
  }
}
